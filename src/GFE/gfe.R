#' Grouped Fixed-Effects estimator of Bonhomme & Manresa (ECTA 2015)
#'
#' @param y Outcome (N x T)
#' @param D Inclusion indicator (N x T)
#' @param groups Number of groups
#' @param Nsim Number of starting values
#' @param Neighbormax Neighborhood size
#' @param stepmax
#' @param VNS Whether to use VNS or simple iterative algorithm
#'
#' @return List consisting of objective value found from each starting
#'         value and assignments and means found from each starting value.
grouped.fixed.effects <- function(y,D=matrix(1,nrow=nrow(y),ncol=ncol(y)),
                                  groups,Nsim=10,
                                  Neighbormax=10,
                                  stepmax=10,
                                  VNS=TRUE)
{
  if (!is.loaded("gfe_r")) {
    dyn.load("gfe.so")
  }
  N <- nrow(y)
  T <- ncol(y)
  if (VNS) {
    VNSnum <- 1
  } else {
    VNSnum <- 0
  }
  y.vec <- as.vector(t(y))

  objective <- rep(1e100,Nsim)
  assignment <- as.integer(matrix(0,nrow=N,ncol=Nsim))
  means <- array(0,dim=c(T,groups,Nsim))

  out <- .Fortran("gfe_r", PACKAGE="gfe",
                  N,T,as.integer(groups),as.integer(Nsim),as.integer(Neighbormax),
                  as.integer(stepmax),as.integer(VNSnum),
                  y.vec,D,objective,as.integer(assignment),means)

  return(list(objective=out[[10]],assignment=matrix(out[[11]],ncol=Nsim),means=out[[12]]))
}





