module imsl
  implicit none
contains
  ! Caution: I do not know the properties of this random number
  ! generator compared to IMSL's drnun.
  subroutine drnun(NR,R)
    implicit none
    integer, intent(in) :: NR
    real(8), intent(inout) :: R(NR)

    call RANDOM_NUMBER(R)
  end subroutine drnun
end module imsl
    
