#include <Rcpp.h>
using namespace Rcpp ;
#include "include/models.hpp"

RCPP_MODULE(stan_fit4em_step1_m_mod) {


    class_<rstan::stan_fit<model_em_step1_m_namespace::model_em_step1_m, boost::random::ecuyer1988> >("model_em_step1_m")

    .constructor<SEXP,SEXP,SEXP>()


    .method("call_sampler", &rstan::stan_fit<model_em_step1_m_namespace::model_em_step1_m, boost::random::ecuyer1988> ::call_sampler)
    .method("param_names", &rstan::stan_fit<model_em_step1_m_namespace::model_em_step1_m, boost::random::ecuyer1988> ::param_names)
    .method("param_names_oi", &rstan::stan_fit<model_em_step1_m_namespace::model_em_step1_m, boost::random::ecuyer1988> ::param_names_oi)
    .method("param_fnames_oi", &rstan::stan_fit<model_em_step1_m_namespace::model_em_step1_m, boost::random::ecuyer1988> ::param_fnames_oi)
    .method("param_dims", &rstan::stan_fit<model_em_step1_m_namespace::model_em_step1_m, boost::random::ecuyer1988> ::param_dims)
    .method("param_dims_oi", &rstan::stan_fit<model_em_step1_m_namespace::model_em_step1_m, boost::random::ecuyer1988> ::param_dims_oi)
    .method("update_param_oi", &rstan::stan_fit<model_em_step1_m_namespace::model_em_step1_m, boost::random::ecuyer1988> ::update_param_oi)
    .method("param_oi_tidx", &rstan::stan_fit<model_em_step1_m_namespace::model_em_step1_m, boost::random::ecuyer1988> ::param_oi_tidx)
    .method("grad_log_prob", &rstan::stan_fit<model_em_step1_m_namespace::model_em_step1_m, boost::random::ecuyer1988> ::grad_log_prob)
    .method("log_prob", &rstan::stan_fit<model_em_step1_m_namespace::model_em_step1_m, boost::random::ecuyer1988> ::log_prob)
    .method("unconstrain_pars", &rstan::stan_fit<model_em_step1_m_namespace::model_em_step1_m, boost::random::ecuyer1988> ::unconstrain_pars)
    .method("constrain_pars", &rstan::stan_fit<model_em_step1_m_namespace::model_em_step1_m, boost::random::ecuyer1988> ::constrain_pars)
    .method("num_pars_unconstrained", &rstan::stan_fit<model_em_step1_m_namespace::model_em_step1_m, boost::random::ecuyer1988> ::num_pars_unconstrained)
    .method("unconstrained_param_names", &rstan::stan_fit<model_em_step1_m_namespace::model_em_step1_m, boost::random::ecuyer1988> ::unconstrained_param_names)
    .method("constrained_param_names", &rstan::stan_fit<model_em_step1_m_namespace::model_em_step1_m, boost::random::ecuyer1988> ::constrained_param_names)
    ;
}
#include <Rcpp.h>
using namespace Rcpp ;
#include "include/models.hpp"

RCPP_MODULE(stan_fit4em_step1_mod) {


    class_<rstan::stan_fit<model_em_step1_namespace::model_em_step1, boost::random::ecuyer1988> >("model_em_step1")

    .constructor<SEXP,SEXP,SEXP>()


    .method("call_sampler", &rstan::stan_fit<model_em_step1_namespace::model_em_step1, boost::random::ecuyer1988> ::call_sampler)
    .method("param_names", &rstan::stan_fit<model_em_step1_namespace::model_em_step1, boost::random::ecuyer1988> ::param_names)
    .method("param_names_oi", &rstan::stan_fit<model_em_step1_namespace::model_em_step1, boost::random::ecuyer1988> ::param_names_oi)
    .method("param_fnames_oi", &rstan::stan_fit<model_em_step1_namespace::model_em_step1, boost::random::ecuyer1988> ::param_fnames_oi)
    .method("param_dims", &rstan::stan_fit<model_em_step1_namespace::model_em_step1, boost::random::ecuyer1988> ::param_dims)
    .method("param_dims_oi", &rstan::stan_fit<model_em_step1_namespace::model_em_step1, boost::random::ecuyer1988> ::param_dims_oi)
    .method("update_param_oi", &rstan::stan_fit<model_em_step1_namespace::model_em_step1, boost::random::ecuyer1988> ::update_param_oi)
    .method("param_oi_tidx", &rstan::stan_fit<model_em_step1_namespace::model_em_step1, boost::random::ecuyer1988> ::param_oi_tidx)
    .method("grad_log_prob", &rstan::stan_fit<model_em_step1_namespace::model_em_step1, boost::random::ecuyer1988> ::grad_log_prob)
    .method("log_prob", &rstan::stan_fit<model_em_step1_namespace::model_em_step1, boost::random::ecuyer1988> ::log_prob)
    .method("unconstrain_pars", &rstan::stan_fit<model_em_step1_namespace::model_em_step1, boost::random::ecuyer1988> ::unconstrain_pars)
    .method("constrain_pars", &rstan::stan_fit<model_em_step1_namespace::model_em_step1, boost::random::ecuyer1988> ::constrain_pars)
    .method("num_pars_unconstrained", &rstan::stan_fit<model_em_step1_namespace::model_em_step1, boost::random::ecuyer1988> ::num_pars_unconstrained)
    .method("unconstrained_param_names", &rstan::stan_fit<model_em_step1_namespace::model_em_step1, boost::random::ecuyer1988> ::unconstrained_param_names)
    .method("constrained_param_names", &rstan::stan_fit<model_em_step1_namespace::model_em_step1, boost::random::ecuyer1988> ::constrained_param_names)
    ;
}
#include <Rcpp.h>
using namespace Rcpp ;
#include "include/models.hpp"

RCPP_MODULE(stan_fit4em_step2_mod) {


    class_<rstan::stan_fit<model_em_step2_namespace::model_em_step2, boost::random::ecuyer1988> >("model_em_step2")

    .constructor<SEXP,SEXP,SEXP>()


    .method("call_sampler", &rstan::stan_fit<model_em_step2_namespace::model_em_step2, boost::random::ecuyer1988> ::call_sampler)
    .method("param_names", &rstan::stan_fit<model_em_step2_namespace::model_em_step2, boost::random::ecuyer1988> ::param_names)
    .method("param_names_oi", &rstan::stan_fit<model_em_step2_namespace::model_em_step2, boost::random::ecuyer1988> ::param_names_oi)
    .method("param_fnames_oi", &rstan::stan_fit<model_em_step2_namespace::model_em_step2, boost::random::ecuyer1988> ::param_fnames_oi)
    .method("param_dims", &rstan::stan_fit<model_em_step2_namespace::model_em_step2, boost::random::ecuyer1988> ::param_dims)
    .method("param_dims_oi", &rstan::stan_fit<model_em_step2_namespace::model_em_step2, boost::random::ecuyer1988> ::param_dims_oi)
    .method("update_param_oi", &rstan::stan_fit<model_em_step2_namespace::model_em_step2, boost::random::ecuyer1988> ::update_param_oi)
    .method("param_oi_tidx", &rstan::stan_fit<model_em_step2_namespace::model_em_step2, boost::random::ecuyer1988> ::param_oi_tidx)
    .method("grad_log_prob", &rstan::stan_fit<model_em_step2_namespace::model_em_step2, boost::random::ecuyer1988> ::grad_log_prob)
    .method("log_prob", &rstan::stan_fit<model_em_step2_namespace::model_em_step2, boost::random::ecuyer1988> ::log_prob)
    .method("unconstrain_pars", &rstan::stan_fit<model_em_step2_namespace::model_em_step2, boost::random::ecuyer1988> ::unconstrain_pars)
    .method("constrain_pars", &rstan::stan_fit<model_em_step2_namespace::model_em_step2, boost::random::ecuyer1988> ::constrain_pars)
    .method("num_pars_unconstrained", &rstan::stan_fit<model_em_step2_namespace::model_em_step2, boost::random::ecuyer1988> ::num_pars_unconstrained)
    .method("unconstrained_param_names", &rstan::stan_fit<model_em_step2_namespace::model_em_step2, boost::random::ecuyer1988> ::unconstrained_param_names)
    .method("constrained_param_names", &rstan::stan_fit<model_em_step2_namespace::model_em_step2, boost::random::ecuyer1988> ::constrained_param_names)
    ;
}
#include <Rcpp.h>
using namespace Rcpp ;
#include "include/models.hpp"

RCPP_MODULE(stan_fit4mle_joint_mod) {


    class_<rstan::stan_fit<model_mle_joint_namespace::model_mle_joint, boost::random::ecuyer1988> >("model_mle_joint")

    .constructor<SEXP,SEXP,SEXP>()


    .method("call_sampler", &rstan::stan_fit<model_mle_joint_namespace::model_mle_joint, boost::random::ecuyer1988> ::call_sampler)
    .method("param_names", &rstan::stan_fit<model_mle_joint_namespace::model_mle_joint, boost::random::ecuyer1988> ::param_names)
    .method("param_names_oi", &rstan::stan_fit<model_mle_joint_namespace::model_mle_joint, boost::random::ecuyer1988> ::param_names_oi)
    .method("param_fnames_oi", &rstan::stan_fit<model_mle_joint_namespace::model_mle_joint, boost::random::ecuyer1988> ::param_fnames_oi)
    .method("param_dims", &rstan::stan_fit<model_mle_joint_namespace::model_mle_joint, boost::random::ecuyer1988> ::param_dims)
    .method("param_dims_oi", &rstan::stan_fit<model_mle_joint_namespace::model_mle_joint, boost::random::ecuyer1988> ::param_dims_oi)
    .method("update_param_oi", &rstan::stan_fit<model_mle_joint_namespace::model_mle_joint, boost::random::ecuyer1988> ::update_param_oi)
    .method("param_oi_tidx", &rstan::stan_fit<model_mle_joint_namespace::model_mle_joint, boost::random::ecuyer1988> ::param_oi_tidx)
    .method("grad_log_prob", &rstan::stan_fit<model_mle_joint_namespace::model_mle_joint, boost::random::ecuyer1988> ::grad_log_prob)
    .method("log_prob", &rstan::stan_fit<model_mle_joint_namespace::model_mle_joint, boost::random::ecuyer1988> ::log_prob)
    .method("unconstrain_pars", &rstan::stan_fit<model_mle_joint_namespace::model_mle_joint, boost::random::ecuyer1988> ::unconstrain_pars)
    .method("constrain_pars", &rstan::stan_fit<model_mle_joint_namespace::model_mle_joint, boost::random::ecuyer1988> ::constrain_pars)
    .method("num_pars_unconstrained", &rstan::stan_fit<model_mle_joint_namespace::model_mle_joint, boost::random::ecuyer1988> ::num_pars_unconstrained)
    .method("unconstrained_param_names", &rstan::stan_fit<model_mle_joint_namespace::model_mle_joint, boost::random::ecuyer1988> ::unconstrained_param_names)
    .method("constrained_param_names", &rstan::stan_fit<model_mle_joint_namespace::model_mle_joint, boost::random::ecuyer1988> ::constrained_param_names)
    ;
}
#include <Rcpp.h>
using namespace Rcpp ;
#include "include/models.hpp"

RCPP_MODULE(stan_fit4mle_step1_mod) {


    class_<rstan::stan_fit<model_mle_step1_namespace::model_mle_step1, boost::random::ecuyer1988> >("model_mle_step1")

    .constructor<SEXP,SEXP,SEXP>()


    .method("call_sampler", &rstan::stan_fit<model_mle_step1_namespace::model_mle_step1, boost::random::ecuyer1988> ::call_sampler)
    .method("param_names", &rstan::stan_fit<model_mle_step1_namespace::model_mle_step1, boost::random::ecuyer1988> ::param_names)
    .method("param_names_oi", &rstan::stan_fit<model_mle_step1_namespace::model_mle_step1, boost::random::ecuyer1988> ::param_names_oi)
    .method("param_fnames_oi", &rstan::stan_fit<model_mle_step1_namespace::model_mle_step1, boost::random::ecuyer1988> ::param_fnames_oi)
    .method("param_dims", &rstan::stan_fit<model_mle_step1_namespace::model_mle_step1, boost::random::ecuyer1988> ::param_dims)
    .method("param_dims_oi", &rstan::stan_fit<model_mle_step1_namespace::model_mle_step1, boost::random::ecuyer1988> ::param_dims_oi)
    .method("update_param_oi", &rstan::stan_fit<model_mle_step1_namespace::model_mle_step1, boost::random::ecuyer1988> ::update_param_oi)
    .method("param_oi_tidx", &rstan::stan_fit<model_mle_step1_namespace::model_mle_step1, boost::random::ecuyer1988> ::param_oi_tidx)
    .method("grad_log_prob", &rstan::stan_fit<model_mle_step1_namespace::model_mle_step1, boost::random::ecuyer1988> ::grad_log_prob)
    .method("log_prob", &rstan::stan_fit<model_mle_step1_namespace::model_mle_step1, boost::random::ecuyer1988> ::log_prob)
    .method("unconstrain_pars", &rstan::stan_fit<model_mle_step1_namespace::model_mle_step1, boost::random::ecuyer1988> ::unconstrain_pars)
    .method("constrain_pars", &rstan::stan_fit<model_mle_step1_namespace::model_mle_step1, boost::random::ecuyer1988> ::constrain_pars)
    .method("num_pars_unconstrained", &rstan::stan_fit<model_mle_step1_namespace::model_mle_step1, boost::random::ecuyer1988> ::num_pars_unconstrained)
    .method("unconstrained_param_names", &rstan::stan_fit<model_mle_step1_namespace::model_mle_step1, boost::random::ecuyer1988> ::unconstrained_param_names)
    .method("constrained_param_names", &rstan::stan_fit<model_mle_step1_namespace::model_mle_step1, boost::random::ecuyer1988> ::constrained_param_names)
    ;
}
