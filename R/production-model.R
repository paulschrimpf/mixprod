## Returns an object of a GNR production model based on the type.
production.model <- function(model = c("cobb.douglas", "translog", "exponential"),
                                 second.stage.instrument.fmla = NULL,
                                 time.varying, T)
{
  LIMIT <- 10
  model <- match.arg(model)
  prod.model <- list()


  if (model == "cobb.douglas")
  {
    logF.fmla <- expression(k*bk + l*bl + m*bm)
    dlogF.func <- deriv(logF.fmla,namevec="m",
                        function.arg=c("m","k","l",
                                       "bm","bk","bl"))

    if (time.varying) {
      prod.fn <- function(param,data) {
        t.min <- min(data$t)
        return(dlogF.func(data$m_it,data$k_it,data$l_it,param[0*T+data$t-t.min+1],
                          param[T+1],param[T+2]))
      }
    } else {
      prod.fn <- function(param,data) {
        return(dlogF.func(data$m_it,data$k_it,data$l_it,param[1],param[2],param[3]))
      }
    }

    n.param <- ifelse(time.varying, T, 1) + 2
    degree <- 1
    prod.fmla <- y_it ~ k_it + l_it + m_it
    share.fmla <- lnmY_it ~ m_it + k_it + l_it
    step2.instruments <- y_it ~ k_it + l_it
    if (!is.null(second.stage.instrument.fmla))
      step2.instruments <- second.stage.instrument.fmla
    if (time.varying)
      step1.param <- c(rep(TRUE, T),FALSE,FALSE)
    else
      step1.param <- c(TRUE, FALSE, FALSE)
    lb <- rep(-1,n.param)
    ub <- rep(1,n.param)
  }
  else if(model == "translog")
  {
    logF.fmla <- expression(k*bk + l*bl +
                              m*(bm + bmm*m + bkm*k + blm*l))# + blm*l + bmm*m))
    dlogF.func <- deriv(logF.fmla,namevec="m",
                        function.arg=c("m","k","l",
                                       "bm","bk","bl","bmm","bkm","blm"))
    trans.fn <- function(param,data=df) {
      extrema <- as.matrix(cbind(1, 2*data$m_it, data$k_it)/data$l_it)
      # if param[6]>=0 then dlogF(data,trans(param))>=0
      trans.p <- param
      trans.p[6] <- param[6] + max(-extrema %*% param[c(1,4:5)], na.rm=TRUE)
      return(trans.p)
    }

    prod.fn <- function(param,data) {
      p <- trans.fn(param,data)
      return(dlogF.func(data$m_it,data$k_it,data$l_it,p[1],p[2],p[3],p[4],p[5],p[6]))
    }

    n.param <- 6
    degree <- 3
    prod.fmla <- y_it ~ k_it + l_it + m_it
    share.fmla <- lnmY_it ~ m_it*(k_it + l_it) + I(m_it^2)
    step2.instruments <- y_it ~ k_it*l_it
    if (!is.null(second.stage.instrument.fmla))
      step2.instruments <- second.stage.instrument.fmla
    step1.param <- c(TRUE,FALSE,FALSE,TRUE,TRUE,TRUE)#,TRUE,TRUE)
    lb <- c(-Inf,-Inf,-Inf,-LIMIT,-LIMIT, 0)#-.1,-.1)
    ub <- c(Inf,  Inf, Inf, LIMIT, LIMIT, LIMIT)#, .1, .1)
  }
  else if (model == "exponential")
  {
    logF.fmla <- expression(k*bk + l*bl +
                              bm*m*exp(bkm*k + blm*l))# + blm*l + bmm*m))
    dlogF.func <- deriv(logF.fmla,namevec="m",
                        function.arg=c("m","k","l",
                                       "bm","bk","bl","bkm","blm"))
    prod.fn <- function(p,data) {
      return(dlogF.func(data$m_it,data$k_it,data$l_it,p[1],p[2],p[3],p[4],p[5]))
    }

    n.param <- 5
    degree <- 3
    is.null()
    prod.fmla <- y_it ~ k_it + l_it + m_it
    share.fmla <- lnmY_it ~ m_it*(k_it + l_it) + I(m_it^2)
    step2.instruments <- y_it ~ k_it*l_it
    if (!is.null(second.stage.instrument.fmla))
      step2.instruments <- second.stage.instrument.fmla
    step1.param <- c(TRUE,FALSE,FALSE,TRUE,TRUE)#,TRUE,TRUE)
    lb=c(-Inf,-Inf,-Inf,-LIMIT,-LIMIT)#,-.1,-.1)
    ub=c(Inf,  Inf, Inf, LIMIT, LIMIT)#, .1, .1)
  }
  else
    stop(paste(model, "model is not currently supported."))

  prod.model <- list(name = model,
                     prod.fn = prod.fn, n.param = n.param,
                     degreee = degree,
                     prod.fmla = prod.fmla,
                     share.fmla = share.fmla,
                     step2.instruments = step2.instruments,
                     step1.param = step1.param,
                     lb = lb, ub = ub)
}

#' Default constructor for a production.model
mixprod.model <- function(parameters, posterior,
                          options) {
  structure(list(parameters=parameters,
                 posterior=posterior,
                 options=options,
                 var=NULL),
            class =  "mixprod.model")
}

print.mixprod.model <- function(model) {
  print(model$parameters)
}


summmary.mixprod.model <- function(model) {
  print("Not implemented yet")
  summa <- structure(model,
                     class=c("summary.mixprod.model",
                             "mixprod.model"))
}

print.summary.mixprod.model <- function(model) {
  print("Not implemented yet")
  print(model$parameters)
}

