# Estimate types given a well-structured dataset. Corresponds to the first stage.
# POST: adds j.hat on panel.df, representing the estimated type
EstimateTypes <- function (panel.df, J,
                           classification.method = c("normal.mix.flexmix","GFE","stan.em"),
                           init.draws = 10,
                           time.varying, flex.labor)
{
  UB.DEFAULT <- 10
  LB.DEFAULT <- -10

  classification.method <- match.arg(classification.method)

  if (classification.method == "GFE")
  {
    if (!is.loaded("gfe_r")) {
      dyn.load("GFE/fortran.modified/gfe.so")
    }
    source("GFE/fortran.modified/gfe.R")
    share <- as.matrix(dcast(data=panel.df, formula=id ~ t,
                             value.var="lnmY_it"))[,-1]
    D <- (!is.na(share))*1
    share[is.na(share)] <- 0
    gfe.mix <-grouped.fixed.effects(share,D=D,groups=J,
                                    Nsim=10,Neighbormax=10,stepmax=10,VNS=TRUE)
    j.hat <- assign.types(gfe.mix, panel.df = ind12)
    panel.df$j.hat <- j.hat
    return (list(mix = gfe.mix, j.hat = j.hat))

  } else if (classification.method=="normal.mix.flexmix") {
    ## use flexmix
    ## note: nrep is used to control # of initial guesses
    norm.mix <- NULL
    if (time.varying)
      norm.mix <- stepFlexmix(lnmY_it ~ as.factor(t) - 1 | id,
                              data = panel.df, k = J, nrep = init.draws,
                              control = list(verbose=1))
    else
      norm.mix <- stepFlexmix(lnmY_it ~ 1 | id,
                              data = panel.df, k = J, nrep = init.draws,
                              control = list(verbose=1))
    j.hat <- assign.types(norm.mix, panel.df = panel.df)
    panel.df$j.hat <- j.hat
    return (list(mix = norm.mix, j.hat = j.hat))
  } else if (classification.method=="stan.em") {
    em.mix <- list(loglike=rep(NA,init.draws), res=list())
    for (i in 1:init.draws) {
      em.mix$res[[i]] <-firstStage.em(data=panel.df, J, time.varying, include.labor=flex.labor)
      em.mix$loglike[i] <- em.mix$res[[i]]$loglike
    }
    j.hat <- assign.types(em.mix, panel.df=panel.df)
    return(list(mix=em.mix, j.hat=j.hat))
  }
}

## Estimate firstStage classifiction by EM using Stan for the M-step.
firstStage.em <- function(data, J, time.varying=FALSE,
                          include.labor=FALSE, tol=1e-4, max.iter=500) {
  init <- list()
  T <- length(unique(data$t))
  for (j in 1:J) {
    init[[j]] <- list(bm = runif(ifelse(time.varying,T,1),0,0.5),
                      bl = runif(ifelse(time.varying,T,1),0,0.5)) ## only used if include.labor==TRUE
    init[[j]]$SigmaEps <- diag(2) ## only used if include.labor==TRUE
    init[[j]]$LEps <- t(chol(init[[j]]$SigmaEps))
    init[[j]]$sigEps <- 1 ## only used if include.labor==FALSE
  }
  if (include.labor) {
    em.stan <- stanmodels$em_step1
  } else {
    em.stan <- stanmodels$em_step1_m
  }
  #em.stan <- stan_model(file="em_step1_m.stan",
  #                      model_name="EM Step 1, M only",
  #                      verbose=TRUE, save_dso=TRUE)#


  data <- data[order(data$id,data$t),]
  stan.data <- list(NT=nrow(data), id=as.numeric(as.factor(data$id)),
                    time=(data$t-min(data$t)+1),
                    nmix=J, N=length(unique(data$id)),
                    T=length(unique(data$t)),
                    m=data$m_it, l=data$l_it,
                    k=data$k_it, y=data$y_it,
                    timeVarying=1L*time.varying)

  #start.time <- proc.time()
  stan.data$w <- matrix(1/J, nrow=stan.data$N, ncol=J)
  loglike0 <- -1e300
  loglike1 <- -1e299
  iter <- 0
  prob <- rep(1/J,J)
  tol <- 1e-2
  max.em.iter <- 100
  parm <- list()
  if (class(init[[1]])!="list") {
    for(j in 1:J) parm[[j]] <- init
  } else {
    parm <- init
  }
  loglike <- matrix(runif(n=stan.data$N*J,min=0.5,max=1), nrow=stan.data$N, ncol=J)
  while(iter < max.em.iter && abs(loglike1-loglike0)>tol) {
    ## e-step
    stan.data$w <- exp(loglike -apply(loglike,1,max)) *
      outer(rep(1,stan.data$N),prob)
    stan.data$w <- stan.data$w/rowSums(stan.data$w)

    ## m-step for type probabilities
    prob <- colMeans(stan.data$w)
    prob
    ## m-step for other parameters
    m <- list()
    for (j in 1:J) {
      if (length(parm[[j]]$bm)==1 && is.null(dim(parm[[j]]$bm))) dim(parm[[j]]$bm) <- 1
      if (include.labor && length(parm[[j]]$bl)==1 && is.null(dim(parm[[j]]$bl))) dim(parm[[j]]$bl) <- 1
      stan.data$j <- j
      invisible(capture.output(
          m[[j]] <- optimizing(em.stan,
                               data=stan.data, init=parm[[j]],
                               algorithm="LBFGS",verbose=TRUE,as_vector=FALSE,hessian=FALSE,
                               iter=100 + iter)
      ))
      parm[[j]] <- m[[j]]$par
      loglike[,j] <- parm[[j]]$loglike_i
    }
    loglike0 <- loglike1
    loglike1 <- sum(log(rowSums(exp(loglike)*outer(rep(1,stan.data$N),prob))))
    if (loglike1<loglike0) {
      print(sprintf("WARNING: EM step decreased likelihood from %f to %f\n",
                    loglike0,loglike1))
    } else {
      print(sprintf("iter %d: loglikelihood = %f\n",iter,loglike1))
    }
    iter <- iter + 1
  }

  #step1.time <- proc.time()
  #print("step 1 time:")
  #print(step1.time-start.time)
  return(list(loglike=loglike1, par=parm, p=loglike))
}
