// Model of first stage likelihood conditional on type when only m is
// flexible. Used for EM estimation.
data {
  int<lower=1> NT;
  int<lower=1> T;  
  int<lower=1> nmix;
  int<lower=1> N;
  int id[NT];
  int time[NT];
  real m[NT];
  real k[NT];
  real l[NT];
  real y[NT];
  real w[N,nmix];
  int<lower=1,upper=nmix> j;
  int<lower=0, upper=1> timeVarying;
}


parameters {
  real<lower=0.0, upper=2.0> bm[1 + timeVarying*(T-1)];
  real<lower=0> sigEps;
}

transformed parameters {
  real loglike_i[N];
    { 
    for (n in 1:NT) {
      int t;
      int i;
      real eps;

      t = time[n];
      i = id[n];
      if (n==1 || id[n]!=id[n-1]) { // new observation
        loglike_i[i] = 0;
      } else {
        if (time[n-1] != t-1) print("ERROR: data not sorted or panel unbalanced.");
      }
    
      eps = log(bm[1 + timeVarying*(t-1)]) + 0.5*sigEps - m[n] + y[n];
      loglike_i[i] = loglike_i[i] + normal_lpdf(eps | 0,sigEps);
    }
  }
}
model {
  for (n in 1:N) {
    target += loglike_i[n]*w[n,j];
  }
}


  
